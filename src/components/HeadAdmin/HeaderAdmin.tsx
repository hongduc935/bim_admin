import { useEffect, useState } from 'react'
import styles from './styles/header-admin.module.scss'
import TotalService from 'api/total.api'
import { NavLink } from 'react-router-dom'
import User from 'constant/User'
import axios from 'axios'
interface Totals {
    countAuths: number,
    countCategories: number
    countBooks: number
    countCallCards: number
}

const HeaderAdmin = () => {

    let [statistical,setStatistical] = useState<Totals>({
        countAuths: 0,
        countCategories: 0,
        countBooks: 0,
        countCallCards: 0
       
    })

    let [countTotalDownloads,setCountTotalDownloads] = useState(0);
    const getStatistical = async () => {
        await TotalService.getAllTotal().then((res: any) => {
            const total = res.data
            setStatistical ( {
                countAuths: total?.auth_count  ?  total.auth_count : 0,
                countCategories: total.linsce_count  ?  total.linsce_count  : 0,
                countBooks: total.key_active  ?  total.key_active : 0,
                countCallCards: total.notification_count  ?  total.notification_count :0
            })
        })
        await axios.post("https://hqltools.com/api/v1/setting/count-download",{}).then((result:any)=>{
            setCountTotalDownloads(result?.data?.data?.totalDownloads )
        })
    }


    useEffect(() => {
        getStatistical();
    }, [])

    return (

        <div className={`${styles.listOverview} item-btw`}>

               <NavLink to={`/${User?.info_user?.role}?Page=Order`} replace={true}>
                <div className={`${styles.overviewItem} item-btw`}>
                <div className={styles.overviewItemIC}>
                        <span><i className="far fa-id-card"></i></span>
                    </div>
                    <div className={styles.overviewItemContent}>
                        <p className={styles.titleItem}>Tài Khoản</p>
                        <p className={styles.count}>{statistical.countAuths}</p>
                    </div>  
              
                  
                </div>
                </NavLink>

                <NavLink to={`/${User?.info_user?.role}?Page=OverView`} replace={true}>
                    <div className={`${styles.overviewItem} item-btw`}>
                        <div className={styles.overviewItemIC}>
                            <span><i className="fa-sharp fa-solid fa-layer-group" /></span>
                        </div>
                        <div className={styles.overviewItemContent}>
                            <p className={styles.titleItem}>Tổng linsce</p>
                            <p className={styles.count}>{statistical.countCategories}</p>
                        </div>

                    </div>
                </NavLink>

                <NavLink to={`/${User?.info_user?.role}?Page=Category`} replace={true}>
                        <div className={`${styles.overviewItem} item-btw`}>
                            <div className={styles.overviewItemIC}>
                                <span><i className="fas fa-book"></i></span>
                            </div>
                            <div className={styles.overviewItemContent}>
                                <p className={styles.titleItem}>Chưa active</p>
                                <p className={styles.count}>{statistical.countBooks}</p>
                            </div>
                        </div>
                </NavLink>

                <NavLink to={`/${User?.info_user?.role}?Page=Account`} replace={true}>
                        <div className={`${styles.overviewItem} item-btw`}>
                            <div className={styles.overviewItemIC}>
                                <span><i className="fas fa-users"></i></span>
                            </div>
                            <div className={styles.overviewItemContent}>
                                <p className={styles.titleItem}>Thông báo</p>
                                <p className={styles.count}>{statistical.countCallCards}</p>
                            </div>
                        </div>
                </NavLink>

                <NavLink to={`/${User?.info_user?.role}?Page=Account`} replace={true}>
                        <div className={`${styles.overviewItem} item-btw`} style={{backgroundColor:"#dadada", borderRadius:"15px", padding:"0px 15px"}}>
                            <div className={styles.overviewItemIC}>
                                <span style={{ fontSize:"20px"}}><i className="fas fa-users"></i></span>
                            </div>
                            <div className={styles.overviewItemContent}>
                                <p className={styles.titleItem} style={{ color:"#ffffff", fontWeight:"bold"}}>Total Download</p>
                                <p className={styles.count} style={{ color:"red", fontWeight:"bold"}}>{countTotalDownloads}</p>
                            </div>
                        </div>
                </NavLink>
        </div>
    )
}

export default HeaderAdmin