import { useState } from "react"
import styles from './styles/sidebar.module.scss'
import { NavLink } from "react-router-dom"
import User from "constant/User"
import Image from "components/Image/Image"

const user = User.info_user
const SideBar = (props: any) => {
    let value = user

    const [colorActive,setColorActive] = useState("activeColor0")

    return (
        <>
            <div className={styles.sidebar}>
                <div className={`${styles.header} item-center `}>
                    <div className={styles.mainheader}>
                        <div className={styles.image}>
                            <Image  contain='cover' image={"./logo.png"} />
                        </div>
                        <div className={styles.name}>{value.name}</div>
                        <div className={`${styles.role} item-center`}>
                            <button><i className="fa-solid fa-registered"/> &nbsp;ADMIN</button>
                        </div>
                    </div>
                </div>
                <div className={styles.menu}>
                    {
                        props?.data.flatMap((value: any, index: number) => {
                            return <NavLink to={`/${props?.path}?Page=${value.path}`} replace={true} key={`sidebar`+index}>
                            <div className={colorActive === `activeColor${index}`? `${styles.item}  activeColor${index}`: `${styles.item}`} onClick={()=>setColorActive(`activeColor${index}`)} key={`${index} menu`} >
                               <div className={styles.mainItem}>
                               <NavLink to={`/${props?.path}?Page=${value.path}`} replace={true}>
                                    <span className={styles.icon1}>
                                        <i className={value.icon}></i>
                                    </span>
                                    <span className={styles.name1}>
                                        {value.title}
                                    </span>
                                </NavLink>
                               </div>
                            </div>
                            </NavLink>
                        })
                    }
                </div>
            </div>
        </>
    )
}
export default SideBar