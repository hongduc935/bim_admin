import styles from './styles/popup-edit-order.module.scss'
import { useState, useEffect } from 'react'
import { Button } from '@mui/material'
import 'react-toastify/dist/ReactToastify.css'
import OrderService from 'api/order.api'
import User from 'constant/User'
import { convertDate } from 'utility/ConvertDate'

export type CallCard = {
    borrower: object,
    libraryClerk: object,
    issueDate: string,
    dueDate: string,
    books: [],
    fee: number,
    active: boolean,
    QRCode: string,
    waitingOrder: boolean,
};



const DetailFeedback = (props: any) => {

    const [isLoading, setIsLoading] = useState<Boolean>(false)
    let user = User.info_user

    // Hứng id detailCallCard
    const { detailCallCard, handleClose } = props;

    const [callcard, setCallCard] = useState<any>(null);

    const [listBook, setListBook] = useState<any[]>([])


    const getCallCardByID = async () => {
        const callcard = await OrderService.getOderByID(detailCallCard);
        await setCallCard(callcard?.data)
        // Convert list books để hiển thị lên table
        let arr: any = [];
        callcard?.data?.books.map((book: any, index: number) => {
            book.idBook.stt = index + 1;
            book.idBook.count = 1;
            arr.push(book.idBook)
        })
        setListBook(arr);

    }
    useEffect(() => {
        getCallCardByID();
    }, [isLoading])

    return (
        <div className={styles.detailScan}>
            <div className={styles.mainPopup}>

                <div className={`${styles.headerPopup} w-90 item-btw`}>
                    <div className={styles.title}>
                        CHI TIẾT FEEDBACK
                    </div>
                    <div className={styles.close} onClick={props?.handleClose}>
                        <i className="fa-sharp fa-solid fa-xmark" />
                    </div>
                </div>

                <div className={`${styles.bodyPopup} w-90`}>

                    
                </div>

            </div >
        </div >
    )
}

export default DetailFeedback

