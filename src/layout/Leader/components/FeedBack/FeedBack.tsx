import { useCallback, useEffect, useMemo, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import styles from './styles/feedback.module.scss'
import OderService from 'api/notification.api'
import FeedbackAPI from 'api/feedback.api'
import MaterialReactTable, {
  MaterialReactTableProps,
  MRT_Cell,
  MRT_ColumnDef,
  MRT_Row,
} from 'material-react-table'
import {
  Box, 
  IconButton,
  Tooltip,
} from '@mui/material'
import { Delete, Edit } from '@mui/icons-material'
import { data } from './makeData'
import DetailFeedback from './components/DetailFeedback/DetailFeedback'
import HeaderAdmin from 'components/HeadAdmin/HeaderAdmin'

export type Person = {
  _id: string
  stt: number
  createdAt: string
  email:string
  content:string
  name:string

};
const Notification = () => {
  const [popupDetailFeedback, setPopupDetailFeedback] = useState<Boolean>(false)
  const [detailOder, setDetailOder] = useState<any>({})
  const [tableData, setTableData] = useState<Person[]>(() => data)
  const [popupAddOder, setPopupAddOder] = useState<Boolean>(false)
  const [validationErrors, setValidationErrors] = useState<{
    [cellId: string]: string
  }>({})

  const handleSaveRowEdits: MaterialReactTableProps<Person>['onEditingRowSave'] =
    async ({ exitEditingMode, row, values }) => {
      if (!Object.keys(validationErrors).length) {
        tableData[row.index] = values;
        setTableData([...tableData]);
        exitEditingMode(); //required to exit editing mode and close modal
      }
    };

  const handleCancelRowEdits = () => {
    setValidationErrors({});
  };

  const handleDeleteRow = useCallback(
    async (row: MRT_Row<Person>) => {
      if (
        !window.confirm(`Bạn chắc chắn muốn xóa thông báo của: ${row.getValue('name')}`)
      ) {
        return;
      }

      const deleteCallCard = async (id:string) => {
        await OderService.onDeleteNotificationID(id);
      }

      await toast.promise(
        deleteCallCard(row.original._id),
        {
          pending: 'Đang xóa thông báo',
          success: 'Đã xóa thông báo thành công 👌',
          error: `Server đang update !`
        },
        {
          autoClose: 3000
        }
      );
      tableData.splice(row.index, 1);
      setTableData([...tableData]);

      setTimeout(() => {
        window.location.reload();
      }, 2500);
    },
    [tableData],
  );

  const getCommonEditTextFieldProps = useCallback(
    (
      cell: MRT_Cell<Person>,
    ): MRT_ColumnDef<Person>['muiTableBodyCellEditTextFieldProps'] => {
      console.log(cell)
      return {
        error: !!validationErrors[cell.id],
        helperText: validationErrors[cell.id],
        onBlur: (event) => {
          const isValid =
            cell.column.id === 'email'
              ? validateEmail(event.target.value)
              : cell.column.id === 'age'
                ? validateAge(+event.target.value)
                : validateRequired(event.target.value);
          if (!isValid) {
            //set validation error for cell if invalid
            setValidationErrors({
              ...validationErrors,
              [cell.id]: `${cell.column.columnDef.header} is required`,
            });
          } else {
            //remove validation error for cell if valid
            delete validationErrors[cell.id];
            setValidationErrors({
              ...validationErrors,
            });
          }
        },
      };
    },
    [validationErrors],
  );

  const columns = useMemo<MRT_ColumnDef<Person>[]>(
    () => [
      {
        accessorKey: 'name',
        header: 'Tên người dùng',
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },

      {
        accessorKey: 'content',
        header: 'Nội dung thông báo',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'createdAt',
        header: 'Ngày Tạo',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'email',
        header: 'Email',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
    ],
    [getCommonEditTextFieldProps],
  );

  let allFeedBack = useCallback(async () => {
    return await FeedbackAPI.getFeedback()
  }, [])


  useEffect(() => {
    allFeedBack().then((result: any) => {
      setTableData(result.data)
    })

  }, [popupAddOder, popupDetailFeedback])

  return (
    <>
      <div className={styles.main}>
        <HeaderAdmin/>
        <MaterialReactTable
          displayColumnDefOptions={{
            'mrt-row-actions': {
              muiTableHeadCellProps: {
                align: 'center',
              },
              size: 120,
            },
          }}
          columns={columns}
          data={tableData}
          // editingMode="modal" //default
          enableColumnOrdering
          enableEditing
          onEditingRowSave={handleSaveRowEdits}
          onEditingRowCancel={handleCancelRowEdits}
          renderRowActions={({ row, table }) => (
            <Box sx={{ display: 'flex', gap: '1rem' }}>
              <Tooltip arrow placement="left" title="Edit">
                <IconButton onClick={() => {
                  setPopupDetailFeedback(true)
                  setDetailOder(row.original._id)
                  console.log("row.original", row.original)
                }}>
                  <Edit />
                </IconButton>
              </Tooltip>
              <Tooltip arrow placement="right" title="Delete">
                <IconButton color="error" onClick={() => handleDeleteRow(row)}>
                  <Delete />
                </IconButton>
              </Tooltip>
            </Box>
          )}
          renderTopToolbarCustomActions={() => (
            <div className={styles.titleTable}> XEM FEEDBACK</div>
          )}
        />
      </div>
      {
        popupDetailFeedback ? <DetailFeedback detailCallCard={detailOder} handleClose={() => setPopupDetailFeedback(false)} /> : <></>
      }
    </>
  )
}
const validateRequired = (value: string) => !!value.length;
const validateEmail = (email: string) =>
  !!email.length &&
  email
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
const validateAge = (age: number) => age >= 18 && age <= 50

export default Notification
