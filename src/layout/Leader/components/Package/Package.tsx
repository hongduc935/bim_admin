import { useCallback, useEffect, useMemo, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import styles from './styles/order.module.scss'
import OderService from 'api/order.api'
import MaterialReactTable, {
  MaterialReactTableProps,
  MRT_Cell,
  MRT_ColumnDef,
  MRT_Row,
} from 'material-react-table'
import {
  Box,
  IconButton,
  Tooltip,
} from '@mui/material';
import { Delete, Edit } from '@mui/icons-material'
import AddPackage from './components/AddLinsceKey/AddPackage'
import PopupEditLinsceKey from './components/PopupEditLinsceKey/PopupEditLinsceKey'
import HeaderAdmin from 'components/HeadAdmin/HeaderAdmin'
import { convertDateFormatMMDDYYY } from 'utility/ConvertDate'
import PackageService from 'api/package.api'
export type Package = {
  _id: string
  name_package:string,
  price_package:Number,
  date_package:Number,
  createdAt: string
}
const Oder = () => {
  const [popupEditOrder, setPopupEditOrder] = useState<Boolean>(false)
  const [detailOder, setDetailOder] = useState<any>({})
  const [tableData, setTableData] = useState<Package[]>(() => [])
  const [popupAddOder, setPopupAddOder] = useState<Boolean>(false)
  const [validationErrors, setValidationErrors] = useState<{
    [cellId: string]: string
  }>({})

  const handleSaveRowEdits: MaterialReactTableProps<Package>['onEditingRowSave'] =
    async ({ exitEditingMode, row, values }) => {
      if (!Object.keys(validationErrors).length) {
        tableData[row.index] = values;

        //send/receive api updates here, then refetch or update local table data for re-render
        setTableData([...tableData]);
        exitEditingMode(); //required to exit editing mode and close modal
      }
    };

  const handleCancelRowEdits = () => {
    setValidationErrors({});
  };

  const handleDeleteRow = useCallback(
    async (row: MRT_Row<Package>) => {
      if (
        !window.confirm(`Bạn chắc chắn muốn xóa linsce key của: ${row.getValue('borrower')}`)
      ) {
        return;
      }

      const deleteCallCard = async (id:string) => {
        await PackageService.onDeletePackageByID(id);
      }

      console.log(row.original._id)
      await deleteCallCard(row.original._id).then((result:any)=>{

        tableData.splice(row.index, 1);
        setTableData([...tableData]);
        toast.success("Đã xóa Package thành công 👌",{
          autoClose: 3000
        });

        if(result?.statusCode === 2000){
          toast.warning(result?.msg,{
            autoClose: 3000
          });
        }
        else if(result?.statusCode === 1000){
          
        }
      })
      .catch((error:any)=>{
        toast.error(error.response.data.msg,{
          autoClose: 3000
        });
      })
    },
    [tableData],
  );

  const getCommonEditTextFieldProps = useCallback(
    (
      cell: MRT_Cell<Package>,
    ): MRT_ColumnDef<Package>['muiTableBodyCellEditTextFieldProps'] => {
      console.log(cell)
      return {
        error: !!validationErrors[cell.id],
        helperText: validationErrors[cell.id],
        onBlur: (event) => {
          const isValid =
            cell.column.id === 'email'
              ? validateEmail(event.target.value)
              : cell.column.id === 'age'
                ? validateAge(+event.target.value)
                : validateRequired(event.target.value);
          if (!isValid) {
            //set validation error for cell if invalid
            setValidationErrors({
              ...validationErrors,
              [cell.id]: `${cell.column.columnDef.header} is required`,
            });
          } else {
            //remove validation error for cell if valid
            delete validationErrors[cell.id];
            setValidationErrors({
              ...validationErrors,
            });
          }
        },
      };
    },
    [validationErrors],
  );

  const columns = useMemo<MRT_ColumnDef<Package>[]>(
    () => [
      {
        accessorKey: 'name_package',
        header: 'Package Name',
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },

      {
        accessorKey: 'price_package',
        header: 'Package Price',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },

      {
        accessorKey: 'date_package',
        header: 'Date Package',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'createdAt',
        header: 'Ngày Tạo',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
    ],
    [getCommonEditTextFieldProps],
  );

  let _fetchAllPackage = useCallback(async () => {
    return await PackageService.getAllPackage()

  }, [])

  useEffect(() => {
    _fetchAllPackage().then((result: any) => {
      if(result && result.data.length > 0 ){
        for(const items of result.data){
            items.createdAt = convertDateFormatMMDDYYY(items?.createdAt)
        }
      }
      setTableData(result.data)
    })
  }, [popupAddOder, popupEditOrder])

  return (
    <>
      <div className={styles.main}>
        <HeaderAdmin/>
        <div className={styles.payBook}>
          <button onClick={() => setPopupAddOder(true)} className={styles.btnAdd}>Add Package</button>
        </div>

        <MaterialReactTable
          displayColumnDefOptions={{
            'mrt-row-actions': {
              muiTableHeadCellProps: {
                align: 'center',
              },
              size: 120,
            },
          }}
          columns={columns}
          data={tableData}
          // editingMode="modal" //default
          enableColumnOrdering
          enableEditing
          onEditingRowSave={handleSaveRowEdits}
          onEditingRowCancel={handleCancelRowEdits}
          renderRowActions={({ row, table }) => (
            <Box sx={{ display: 'flex', gap: '1rem' }}>
              <Tooltip arrow placement="left" title="Edit">
                <IconButton onClick={() => {
                  setPopupEditOrder(true)
                  setDetailOder(row.original._id)
                }}>
                  <Edit />
                </IconButton>
              </Tooltip>
              <Tooltip arrow placement="right" title="Delete">
                <IconButton color="error" onClick={() => handleDeleteRow(row)}>
                  <Delete />
                </IconButton>
              </Tooltip>
            </Box>
          )}
          renderTopToolbarCustomActions={() => (
            <div className={styles.titleTable}> DANH SÁCH LINSCE KEY</div>
          )}
        />
      </div>
      {
        popupAddOder ? <AddPackage handleClose={() => setPopupAddOder(false)} /> : <></>
      }
      {
        popupEditOrder ? <PopupEditLinsceKey linscekeyID={detailOder} handleClose={() => setPopupEditOrder(false)} /> : <></>
      }
    </>
  )
}
const validateRequired = (value: string) => !!value.length;
const validateEmail = (email: string) =>
  !!email.length &&
  email
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
const validateAge = (age: number) => age >= 18 && age <= 50

export default Oder
