import styles from './styles/add-package.module.scss'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useForm, SubmitHandler } from "react-hook-form"
import PackageService from 'api/package.api'

type Inputs = {
  name_package: String,
  price_package: Number,
  date_package:Number
};


const AddPackage = (props: any) => {

    const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> =async (data) => {
        let result:any = await PackageService.onCreatePackage(data)
        console.log(result);
        if(result?.statusCode === 1000){
            toast.success(result?.msg);
            setTimeout(()=>{
                props?.handleClose()
                window.location.reload();
            },1500)
        }
        else{
            toast.warning(result?.msg);
        }  
    }

    return (
        <div className={styles.addOder}>
            <div className={styles.addOderMain}>
                <div className={`${styles.header} w-90 item-btw`}>
                    <div className={styles.title}>
                        Thêm Package
                    </div>
                    <div className={styles.close} onClick={props?.handleClose}>
                        <i className="fa-sharp fa-solid fa-xmark" />
                    </div>
                </div>

                <div className={styles.body}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className = {styles.items} >
                            <p>Name Package</p>
                            <input type={"text"} {...register("name_package",{ required: true})} />
                        </div>
                        <div className = {styles.items} >
                            <p>Price Package</p>
                            <input type={"number"} {...register("price_package",{ required: true})}/>
                        </div>
                        <div className = {styles.items} >
                            <p>Date Package</p>
                            <input type={"number"} {...register("date_package",{ required: true})}/>
                        </div>
                        
                        <div className = {styles.items} >
                            <button type='submit'>Add Package</button>
                        </div>
                    </form>
                    
                </div >
            </div >
        </div >
    )
}

export default AddPackage