import styles from './styles/popup-edit-linscekey.module.scss'
import { useState, useEffect } from 'react'
import { Button } from '@mui/material'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import OrderService from 'api/order.api'
import { convertDate } from 'utility/ConvertDate'
import { useForm, SubmitHandler } from "react-hook-form"


type Inputs = {
    exprire:string
    id?:string
    status:Number
};

const PopupEditLinsceKey = (props: any) => {


    const { register, handleSubmit, formState: { errors } } = useForm<Inputs>()



    const onSubmit: SubmitHandler<Inputs> = async(data:any) => {
        data.id = linscekeyID
        data.status = statusKey
        await OrderService.extendLinsceKeyUpdate(data).then((resultExtend:any)=>{

            if(resultExtend?.statusCode === 1000){
                toast.success(resultExtend?.msg);
                setTimeout(()=>{
                    props?.handleClose()
                },1500)
            }
            else{
                toast.warning(resultExtend?.msg);
            }  
        })
        .catch((error:any)=>{
            toast.error(error.response.data.msg,{
              autoClose: 3000
            });
          })

       
    }

    const [isLoading, setIsLoading] = useState<Boolean>(false)
    const [statusKey, setStatusKey] = useState<Number>(0)
    const [linscekey, setLinsceKey] = useState<any>({phone:"",email:"",exprire:"",keyHash:"",status:"",createdAt:""})

    // Hứng id linscekeyID
    let { linscekeyID, handleClose } = props

    const getLinsceKeyByID = async () => {

        const linscekey_record:any = await OrderService.getOderByID(linscekeyID);

        setLinsceKey(linscekey_record?.data)
        setStatusKey(linscekey_record?.data?.status === true ? 1 :0)

    }

    useEffect(() => {
        getLinsceKeyByID();
    }, [isLoading])

    return (
        <div className={styles.detailScan}>
            <div className={styles.mainPopup}>
                <div className={`${styles.headerPopup} w-90 item-btw`}>
                    <div className={styles.title}>
                        THÔNG TIN LINSCE KEY
                    </div>
                    <div className={styles.close} onClick={props?.handleClose}>
                        <i className="fa-sharp fa-solid fa-xmark" />
                    </div>
                </div>

                <div className={`${styles.bodyPopup} w-90`}>
                    <div className={`${styles.result} w-90`}>
                        <form onSubmit={handleSubmit(onSubmit)} >
                            <div className={styles.main}>
                                <div>
                                    <div className={styles.flexItems}>
                                        <div className={styles.loginFormBodyItem}>
                                            <input type={"text"}
                                                defaultValue={linscekey?.phone?.length > 0 ? linscekey?.phone : "" }
                                                disabled
                                            />
                                            <span>Phone</span>
                                        </div>
                                        <div className={styles.loginFormBodyItem}>
                                            <input
                                                type={"text"}
                                                defaultValue={linscekey?.email?.length > 0 ? linscekey?.email : "" }
                                                disabled
                                            />
                                            <span>Email</span>
                                        </div>
                                    </div>

                                    <div className={styles.flexItems}>
                                        
                                        <div className={styles.loginFormBodyItem}>
                                            <input type={"text"}
                                                defaultValue={linscekey?.keyHash?.length > 0 ? linscekey?.keyHash : ""}
                                                disabled
                                            />
                                            <span>Key Hash</span>
                                        </div>
                                        <div className={styles.loginFormBodyItem}>
                                            <input type={"text"}
                                                defaultValue={linscekey.createdAt ? convertDate(linscekey?.createdAt) : ""}
                                            />
                                            <span>Ngày tạo</span>
                                        </div>
                                    </div>

                                    <div className={styles.flexItems}>
                                        <div className={styles.loginFormBodyItem}>
                                            <select   onChange={(e:any)=>setStatusKey(e.target.value)} >
                                            <option value={2}>{linscekey?.status ===true ? " --- Kích hoạt ---" : " ---Chưa kích hoạt---"}</option>
                                                <option value={1}>Kích hoạt</option>
                                                <option value={0}>Chưa kích hoạt</option>
                                            </select>
                                            <span>Status</span>
                                        </div>
                                        <div className={styles.loginFormBodyItem}>
                                            <input type={"text"}
                                                defaultValue={0}
                                                {...register("exprire", { required: true })}
                                            />
                                            <span>Cập nhật</span>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className={styles.groupBtn}>
                                <Button
                                    style={{ minWidth: "110px" }}
                                    variant='contained'
                                    color='warning'
                                    size="medium"
                                    onClick={handleClose}
                                >Hủy</Button>
                                <Button
                                    style={{ minWidth: "110px" }}
                                    variant='contained'
                                    size="medium"
                                    color='success'
                                    type='submit'
                                >Gia hạn</Button>

                            </div>

                        </form>

                    </div>
                </div>

            </div >
            <ToastContainer/>
        </div >
    )
}

export default PopupEditLinsceKey

