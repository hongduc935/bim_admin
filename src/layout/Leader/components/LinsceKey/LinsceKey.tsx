import { useCallback, useEffect, useMemo, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import styles from './styles/order.module.scss'
import OderService from 'api/order.api'
import MaterialReactTable, {
  MaterialReactTableProps,
  MRT_Cell,
  MRT_ColumnDef,
  MRT_Row,
} from 'material-react-table'
import {
  Box,
  IconButton,
  Tooltip,
} from '@mui/material';
import { Delete, Edit } from '@mui/icons-material'
import { data } from './makeData'
import AddOder from './components/AddLinsceKey/AddLinsceKey'
import PopupEditLinsceKey from './components/PopupEditLinsceKey/PopupEditLinsceKey'
import HeaderAdmin from 'components/HeadAdmin/HeaderAdmin'
import { convertDateFormatMMDDYYY } from 'utility/ConvertDate'
export type Person = {
  _id: string
  stt: number
  createdAt: string
  email:string,
  phone:String,
  exprire:string,
  keyHash:string
  serial:string
  status_exprire:string

}
const Oder = () => {
  const [popupEditOrder, setPopupEditOrder] = useState<Boolean>(false)
  const [detailOder, setDetailOder] = useState<any>({})
  const [tableData, setTableData] = useState<Person[]>(() => data)
  const [popupAddOder, setPopupAddOder] = useState<Boolean>(false)
  const [validationErrors, setValidationErrors] = useState<{
    [cellId: string]: string
  }>({})

  const handleSaveRowEdits: MaterialReactTableProps<Person>['onEditingRowSave'] =
    async ({ exitEditingMode, row, values }) => {
      if (!Object.keys(validationErrors).length) {
        tableData[row.index] = values;
        // console.log("Data sau khi edit: ", tableData[row.index])

        //send/receive api updates here, then refetch or update local table data for re-render
        setTableData([...tableData]);
        exitEditingMode(); //required to exit editing mode and close modal
      }
    };

  const handleCancelRowEdits = () => {
    setValidationErrors({});
  };

  const handleDeleteRow = useCallback(
    async (row: MRT_Row<Person>) => {
      if (
        !window.confirm(`Bạn chắc chắn muốn xóa linsce key của: ${row.getValue('borrower')}`)
      ) {
        return;
      }

      const deleteCallCard = async (id:string) => {
        await OderService.onDeleteLinsceByID(id);
      }

      console.log(row.original._id)
      await deleteCallCard(row.original._id).then((result:any)=>{

        console.log("JSON:"+JSON.stringify(result));

        tableData.splice(row.index, 1);
        setTableData([...tableData]);
        toast.success("Đã xóa linsce key thành công 👌",{
          autoClose: 3000
        });

        if(result?.statusCode === 2000){
          
          toast.warning(result?.msg,{
            autoClose: 3000
          });
        }
        else if(result?.statusCode === 1000){
          
        }
      })
      .catch((error:any)=>{

        toast.error(error.response.data.msg,{
          autoClose: 3000
        });
      })
      // await toast.promise(
      //   deleteCallCard(row.original._id),
      //   {
      //     pending: 'Đang xóa linsce key',
      //     success: 'Đã xóa linsce key thành công 👌',
      //     error: `Không thể xóa phiếu mượn, phiếu có thể chưa trả,
      //     Vui lòng khiểm tra lại !`
      //   },
      //   {
      //     autoClose: 3000
      //   }
      // );
    
    },
    [tableData],
  );

  const getCommonEditTextFieldProps = useCallback(
    (
      cell: MRT_Cell<Person>,
    ): MRT_ColumnDef<Person>['muiTableBodyCellEditTextFieldProps'] => {
      console.log(cell)
      return {
        error: !!validationErrors[cell.id],
        helperText: validationErrors[cell.id],
        onBlur: (event) => {
          const isValid =
            cell.column.id === 'email'
              ? validateEmail(event.target.value)
              : cell.column.id === 'age'
                ? validateAge(+event.target.value)
                : validateRequired(event.target.value);
          if (!isValid) {
            //set validation error for cell if invalid
            setValidationErrors({
              ...validationErrors,
              [cell.id]: `${cell.column.columnDef.header} is required`,
            });
          } else {
            //remove validation error for cell if valid
            delete validationErrors[cell.id];
            setValidationErrors({
              ...validationErrors,
            });
          }
        },
      };
    },
    [validationErrors],
  );

  const columns = useMemo<MRT_ColumnDef<Person>[]>(
    () => [
      {
        accessorKey: 'stt',
        header: 'STT',
        enableColumnOrdering: false,
        enableEditing: false, //disable editing on this column
        enableSorting: false,
        size: 80,
      },

      {
        accessorKey: 'email',
        header: 'Email',
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },

      {
        accessorKey: 'phone',
        header: 'Số Điện Thoại',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'createdAt',
        header: 'Ngày Tạo',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'exprire',
        header: 'Thời Hạn',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'serial',
        header: 'Mã Máy',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'keyHash',
        header: 'LinsceKey',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'status_exprire',
        header: 'Hết hạn',
        size: 140,
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
    ],
    [getCommonEditTextFieldProps],
  );

  let db = useCallback(async () => {
    return await OderService.getAllLinsceKey()
  }, [])

  useEffect(() => {
    db().then((result: any) => {
      for(const items of result.data){

          let result_status = new Date(convertDateFormatMMDDYYY(items?.createdAt)).getTime() + Number(items?.exprire)* 86400000 < new Date().getTime()  ? "Hết hạn":"Còn sử dụng"

          items.status_exprire = result_status
      }

      setTableData(result.data)
    })

  }, [popupAddOder, popupEditOrder])

  return (
    <>
      <div className={styles.main}>
        <HeaderAdmin/>
        <div className={styles.payBook}>
          <button onClick={() => setPopupAddOder(true)} className={styles.btnAdd}>Thêm Key</button>
        </div>

        <MaterialReactTable
          displayColumnDefOptions={{
            'mrt-row-actions': {
              muiTableHeadCellProps: {
                align: 'center',
              },
              size: 120,
            },
          }}
          columns={columns}
          data={tableData}
          // editingMode="modal" //default
          enableColumnOrdering
          enableEditing
          onEditingRowSave={handleSaveRowEdits}
          onEditingRowCancel={handleCancelRowEdits}
          renderRowActions={({ row, table }) => (
            <Box sx={{ display: 'flex', gap: '1rem' }}>
              <Tooltip arrow placement="left" title="Edit">
                <IconButton onClick={() => {
                  setPopupEditOrder(true)
                  setDetailOder(row.original._id)
                }}>
                  <Edit />
                </IconButton>
              </Tooltip>
              <Tooltip arrow placement="right" title="Delete">
                <IconButton color="error" onClick={() => handleDeleteRow(row)}>
                  <Delete />
                </IconButton>
              </Tooltip>
            </Box>
          )}
          renderTopToolbarCustomActions={() => (
            <div className={styles.titleTable}> DANH SÁCH LINSCE KEY</div>
          )}
        />
      </div>
      {
        popupAddOder ? <AddOder handleClose={() => setPopupAddOder(false)} /> : <></>
      }
      {
        popupEditOrder ? <PopupEditLinsceKey linscekeyID={detailOder} handleClose={() => setPopupEditOrder(false)} /> : <></>
      }
    </>
  )
}
const validateRequired = (value: string) => !!value.length;
const validateEmail = (email: string) =>
  !!email.length &&
  email
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
const validateAge = (age: number) => age >= 18 && age <= 50

export default Oder
