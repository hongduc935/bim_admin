import styles from './styles/add-linscekey.module.scss'
import { Response } from '@type/ListResponse'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useForm, SubmitHandler } from "react-hook-form"
import OderService from 'api/order.api'

type Inputs = {
  name: string,
  email: string,
  phone:string,
  packages:number
};


const AddOder = (props: any) => {

    const { register, handleSubmit, watch, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> =async (data) => {
        let result:any = await OderService.onCreateLinsceKey(data)
        console.log(result);
        if(result?.statusCode === 1000){
            toast.success(result?.msg);
            setTimeout(()=>{
                props?.handleClose()
                window.location.reload();
            },1500)
        }
        else{
            toast.warning(result?.msg);
        }  
        console.log(data);
    }

    return (
        <div className={styles.addOder}>
            <div className={styles.addOderMain}>
                <div className={`${styles.header} w-90 item-btw`}>
                    <div className={styles.title}>
                        Thêm Key
                    </div>
                    <div className={styles.close} onClick={props?.handleClose}>
                        <i className="fa-sharp fa-solid fa-xmark" />
                    </div>
                </div>

                <div className={styles.body}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className = {styles.items} >
                            <p>Name</p>
                            <input type={"text"} {...register("name")}/>
                        </div>
                        <div className = {styles.items}>
                            <p>Email</p>
                            <input type={"text"} {...register("email" ,{ required: true, pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ })}/>
                        </div>
                        <div className = {styles.items} >
                            <p>Phone</p>
                            <input type={"text"} {...register("phone",{ required: true,minLength:10,maxLength:11 })}/>
                        </div>
                        <div className = {styles.items}>
                            <p>Gói</p>
                            <select {...register("packages")}>
                                    <option >Gói đăng kí</option>
                                    <option value={30}>1 tháng (trial free)</option>
                                    <option value={180}>6 tháng (Fee)</option>
                                    <option value={360}>12 tháng (Fee)</option>
                                    <option value={510}>18 tháng (Fee)</option>
                                </select>
                        </div>
                        <div className = {styles.items} >
                            <button>Tạo LinsceKey</button>
                        </div>
                    </form>
                    
                </div >
            </div >
        </div >
    )
}

export default AddOder