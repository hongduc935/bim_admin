import styles from './styles/sidebar-register.module.scss'
import { useCallback, useEffect, useState } from 'react'
import { useForm, SubmitHandler } from "react-hook-form"
import { toast } from 'react-toastify'
import { useAppDispatch } from 'redux/store'
import { IRegister } from '@type/AuthInterface'
import { RegisterAction } from 'redux/action/Auth/Auth'
import PackageService from 'api/package.api'

const SideBarRegister = (props: any) => {

    const dispatch = useAppDispatch()

    const notify = (content: string) => toast(`${content}`!)

    const { register, handleSubmit, formState: { errors } } = useForm<IRegister>()

    let [packages, setPackages] = useState<any>(null)

    const _fetchPackage = useCallback(async () => {
        return  await PackageService.getAllPackage();
     }, [])

     useEffect(()=>{

        _fetchPackage().then((res:any)=>{
            setPackages(res?.data)
        })

    },[])


    useEffect(() => {

        if (errors?.email || errors?.password) {
            notify(" Validate Form Fail !!!")
        }

    }, [errors])
    const onSubmit: SubmitHandler<IRegister> = async (data: any, e: any) => {
        try {
            console.log("Auth", data)
            await dispatch(RegisterAction(data))

        } catch (e) {
            notify("Register Fail")
        }
    };

    return (

        <div className={styles.sidebarEdit}>
            <div className={styles.containerEdit}>
                <form onSubmit={handleSubmit(onSubmit)} >
                    <div className={styles.headerEdit}>
                        <div className={styles.close} onClick={props.handleClose}>
                            <i className="fa-sharp fa-solid fa-xmark" />
                        </div>
                    </div>
                    <div className={styles.loginForm}>
                    <div className={styles.loginFormTitle}>
                        <h2>Đăng kí tài khoản</h2>
                    </div>
                    <div className={styles.loginFormBody}>
                        <div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your name'
                                       {...register("name",{ required: true })}
                                />
                                <span>{errors?.name&& "Name is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"password"} placeholder='Enter your password'
                                       {...register("password",{ required: true,minLength:6 })}
                                />
                                <span>{errors?.password&& "Password is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your phone'
                                       {...register("phone",{ required: true,minLength:10,maxLength:11 })}
                                />
                                <span>{errors?.phone&& "Phone is validate *"} </span>
                            </div>
                        </div>
                        <div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your email'
                                       {...register("email",{required:true,pattern:/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/})}
                                />
                                <span>{errors?.email&& "Email is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"password"} placeholder='Enter confirm password'/>
                                <span>{errors?.password&& "Password is validate *"} </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <select {...register("packages")}>
                                    <option value={-1}>Gói đăng kí</option>
                                    {packages && packages?.length > 0 ? packages.flatMap((value:any,index:number)=>{
                                        return  <option value={value?.date_package ? value?.date_package : 360} key={`package${index}`}>
                                                    {value?.name_package} ({value?.price_package} VND)
                                                </option>
                                    }) :<>
                                    </>}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className={styles.loginFormBottom}>
                        <div className={styles.loginFormBottomAction}>
                            <button > Register</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    )
}

export default SideBarRegister