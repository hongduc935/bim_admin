import  { useCallback, useEffect, useState } from 'react'
import styles from './styles/add-oder.module.scss'
import OderService from 'api/order.api'
import { Response } from '@type/ListResponse'
import NotificationService from 'api/notification.api'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { ToastContainer } from 'react-toastify'
import { useForm, SubmitHandler } from "react-hook-form"
import {  convertDateFormatMMDDYYY } from 'utility/ConvertDate'
type Inputs = {
  title: string
  content: string
  type:string
  list_user?:string[],
  start:string,
  end:string
};

const AddNotification = (props: any) => {

    const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = async(data) => {
        let list_user = []
        for(const item of linsces){
            list_user.push(item.email)
        }
        data.list_user = list_user
        let result:any = await NotificationService.onCreateNotification(data)
        if(result?.statusCode === 1000){
            toast.success(result?.msg);
            setTimeout(()=>{
                props?.handleClose()
                window.location.reload();
            },1500)
        }
        else{
            toast.warning(result?.msg);
        }  
    }

    let [linsces,setLinsces] = useState<any>([])

    const fetchLinsces = useCallback(async()=>{
        return await OderService.getAllLinsceKey()
    },[])

    const handleChangeTypeNotification = (type:string)=>{
        let filterLinsces = []
        if(type === "exprire"){
            filterLinsces = linsces.filter((items:any)=>new Date(convertDateFormatMMDDYYY(items?.createdAt)).getTime() + Number(items?.exprire)* 86400000 < new Date().getTime())
            setLinsces(filterLinsces)
        }
    }

    useEffect(()=>{
        fetchLinsces().then((result:any)=>{
            setLinsces(result?.data)
        })
    },[])


    return (
        <div className={styles.addOder}>
            <div className={styles.addOderMain}>
                <div className={`${styles.header} w-90 item-btw`}>
                    <div className={styles.title}>
                        Thêm thông báo
                    </div>
                    <div className={styles.close} onClick={props?.handleClose}>
                        <i className="fa-sharp fa-solid fa-xmark" />
                    </div>
                </div>

                <div className={styles.body}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className = {styles.items}>
                            <p>Tiêu đề</p>
                            <input type={"text"} {...register("title", { required: true })}/>
                        </div>
                        <div className = {styles.items} >
                            <p>Nội dung</p>
                            <textarea placeholder='Nhập nội dung thông báo' {...register("content", { required: true })}/>
                        </div>
                        <div className = {styles.items} >
                            <p>Ngày bắt đầu</p>
                            <input type={"datetime-local"}  {...register("start", { required: true })}/>
                        </div>
                        <div className = {styles.items} >
                            <p>Ngày kết thúc</p>
                            <input type={"datetime-local"}  {...register("end", { required: true })}/>
                        </div>
                        <div className = {styles.items} >
                            <p>Loại thông báo</p>
                            <select {...register("type", { required: true })} defaultValue={"--None--"} onChange={(event)=>handleChangeTypeNotification(event.target.value)}>
                                <option>--None--</option>
                                <option value={"version"}>Update Version</option>
                                <option value={"exprire"}>Hết Hạn Linscekey</option>
                            </select>
                        </div>
                        <div className = {styles.items} >
                            <button>Tạo thông báo</button>
                        </div>
                    </form>
                    
                </div >
            </div >
            <ToastContainer/>
        </div >
    )
}

export default AddNotification