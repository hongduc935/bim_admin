interface ISideBar {
    path: String
    title: String
    icon:String
}
const DataSidebarAdmin: ISideBar[] = [
    {
        path: "Home",
        title: "Home",
         icon:"fa-regular fa-book"
    },
    {
        path: "Teacher",
        title: "Teacher",
        icon:"fa-regular fa-book"
    },
    {
        path: "Student",
        title: "Teacher",
        icon:"fa-regular fa-book"
    },
    {
        path: "Account",
        title: "Account",
        icon:"fa-regular fa-book"
    },
]
const DataSidebarTeacher: ISideBar[] = [
    {
        path: "Topic",
        title: "Đề tài",
        icon:"fa-regular fa-book"
    },
    {
        path: "GroupStudent",
        title: "Sinh viên",
        icon:"fa-regular fa-book"
    },
    {
        path: "ViewReport",
        title: "Báo cáo",
        icon:"fa-regular fa-book"
    },
    {
        path: "Core",
        title: "Điểm số",
        icon:"fa-regular fa-book"
    },
    {
        path: "Profile",
        title: "Cá nhân",
        icon:"fa-regular fa-book"
    }
]
const DataSidebarLeader: ISideBar[] = [
    {
        path: "Account",
        title: "Tài khoản",
        icon:"fa-regular fa-user"
    },
    {
        path: "LinsceKey",
        title: "Linsce Key",
        icon:"fa-solid fa-key"
    },
    {
        path: "Package",
        title: "Package",
        icon:"fa-solid fa-cubes"

    },
    {
        path: "FeedBack",
        title: "Feed Back",
        icon:"fa-solid fa-comment"
    },
    {
        path: "Notification",
        title: "Notification",
        icon:"fa-regular fa-bell"
    },
    {
        path: "Profile",
        title: "Cá nhân",
        icon:"fa-regular fa-user"
    },

]
const DataSidebarStudent: ISideBar[] = [

    {
        path: "Book",
        title: "Sách",
        icon:"fa-regular fa-book"
    },
    {
        path: "Oder",
        title: "Đơn Mượn",
        icon:"fa-regular fa-book"
    },
    {
        path: "Profile",
        title: "Cá nhân",
        icon:"fa-regular fa-book"
    },
    {
        path: "Policy",
        title: "Chính sách",
        icon:"fa-regular fa-book"
    },
    // {
    //     path: "FeedBack",
    //     title: "Y Kiến",
    //     icon:"fa-regular fa-book"
    // },
]

const SideBarDB = {
    DataSidebarAdmin,
    DataSidebarLeader,
    DataSidebarStudent,
    DataSidebarTeacher
}
export default SideBarDB
