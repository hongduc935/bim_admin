import { lazy } from 'react'
const Auth=lazy(() => import("pages/Auth/Auth"))
const Home = lazy(() => import("pages/Home/Home"))
const routes = [
    // {
    //     path: '/login',
    //     component: Home,
    //     exact: true
    // },
    {
        path: '/',
        component: Auth,
        exact: true
    },
   
];

export default routes;
