import { Navigate } from 'react-router-dom'
import React from 'react'
import Storage from "congfig/storage/Storage"
function AuthenticatedGuard({children,...rest}:any) {

    console.log("OKeeee")

    let isLogin = Storage.GetLocalStorage("user")

    let isRole = JSON.parse(Storage.GetLocalStorage("info-user" )|| "") || {}

    let role:string = isRole?.user?.role

    // console.log(JSON.stringify(role))

    return (
        <>
            { isLogin && role === "admin"  || isLogin && role === "manage" ? children: <Navigate to="/" replace /> }
        </>
    )
}

export default AuthenticatedGuard;