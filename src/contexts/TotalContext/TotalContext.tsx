import AuthServices from 'api/auth.api'
import  { createContext, useState,PropsWithChildren, useEffect, useCallback } from 'react'
import { Response } from '@type/ListResponse'

interface Totals {
  account:number
  topic:number
  assembly:number
  group:number
}
  
const AppCtx = createContext<Totals | null>({
  topic: 0,
  account: 0,
  group: 0,
  assembly:0
});
  
function ContextTotalProvider(props:PropsWithChildren<{}>) {

  const [totals,setTotals] = useState<Totals>({account:0,topic:0,assembly:0,group:0})

  const callTotalAccount = useCallback(async()=>{
    return await AuthServices.getTotalAccount()
  },[])
  useEffect(()=>{
    callTotalAccount().then((data:Response|undefined)=>{
      setTotals((prevState)=>{return {...prevState,account:data?.data}})
    })
  },[])

    return <AppCtx.Provider value={totals} {...props}/>
}

export default ContextTotalProvider;