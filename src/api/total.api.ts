import Repository from "../congfig/repository/RepositoryConfig"
import { DomainServer } from "constant/Constant"
const url=DomainServer+'/api/v1/share/count-model'
// const url = 'http://localhost:5000/api/v1/share/count-model'

const TotalService = {
    getAllTotal: async () => {
        return await Repository("POST", url, { params: {}, payload: {} })
    }
}
export default TotalService