import Repository from "../congfig/repository/RepositoryConfig"
import { DomainServer } from "constant/Constant"
const url=DomainServer+'/api/v1/package'

const PackageService = {
    getAllPackage: async () => {
        return await Repository("POST", url, { params: {}, payload: {} })
    },
    onCreatePackage: async (data: any) => {
        return await Repository("POST", url+"/create", { params: {}, body: data })
    },
    onDeletePackageByID: async (id: string) => {
        return await Repository("DELETE", url + `/${id}`, { params: {}, body: {} })
    },
    onUpdatPackageByID: async ({ data, id }: any) => {
        return await Repository("PUT", url + `/${id}`, { params: {}, body: data })
    },
   
}
export default PackageService