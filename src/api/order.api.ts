import Repository from "../congfig/repository/RepositoryConfig"
import { DomainServer } from "constant/Constant"
const url=DomainServer+'/api/v1/linsce'
// const url = 'http://localhost:5000/api/v1/linsce'

const OderService = {
    getAllLinsceKey: async () => {
        return await Repository("POST", url, { params: {}, payload: {} })
    },
    onCreateLinsceKey: async (data: any) => {
        return await Repository("POST", url+"/create", { params: {}, body: data })
    },
    onDeleteLinsceByID: async (id: string) => {
        // console.log(data)
        return await Repository("DELETE", url + `/${id}`, { params: {}, body: {} })
    },
    onUpdateOderByID: async ({ data, id }: any) => {
        return await Repository("PUT", url + `/${id}`, { params: {}, body: data })
    },
    extendLinsceKeyUpdate: async (data : any) => {
        return await Repository("POST", url + `/extends`, { params: {}, body: data })
    },
    getOderByID: async (id: string) => {
        return await Repository("POST", url + `/detail`, { params: {}, body: {id} })
    },

    returnOderByID: async (id: string) => {
        return await Repository("PUT", url + `/${id}`, { params: {}, body: {} })
    },
 

}
export default OderService