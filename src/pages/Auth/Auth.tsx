import React, { useEffect, useState } from 'react'
import ComponentPage from "components/ComponentPage/ComponentPages"
import styles from './styles/auth.module.scss'
import LoginComponent from "pages/Auth/components/Login/LoginComponent"
import ForgotPassword from "pages/Auth/components/ForgotPassword/ForgotPassword"
import LogoSPKT from 'asset/images/logo-graduation.jpg'
import Image from "components/Image/Image"
const Auth=()=>{

    const [isLogin,setIsLogin]=React.useState("login")

    function HandleChangLoginForgot(){
        setIsLogin("forgot")
    }
   
    return(
        <ComponentPage isHiddenHeader={false} isHiddenFooter={false}>
            
            <div className={`${styles.loginContainer} item-center`}>
                <div className={`${styles.loginMain}`}>
                    <div className={styles.img}>
                        <div className={styles.logo}>
                            <Image image={LogoSPKT}/>
                        </div>
                    </div>
                    <div className={styles.content}>
                        {isLogin==="login" && <LoginComponent  HandleChangLoginForgot={HandleChangLoginForgot}v  />}
                        {isLogin==="forgot" && <ForgotPassword  setIsLogin={setIsLogin}/>}
                    </div>
                </div>
            </div>
        </ComponentPage>
    )
}
export default  Auth